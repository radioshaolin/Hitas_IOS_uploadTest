//
//  ViewController.swift
//  Hitas_IOS_uploadTest
//
//  Created by Rkhorenko on 8/16/17.
//  Copyright © 2017 Rkhorenko. All rights reserved.
//

import UIKit
import Alamofire
import Zip
import UICircularProgressRing

class ViewController: UIViewController, UICircularProgressRingDelegate  {
    
    @IBOutlet weak var ring1: UICircularProgressRingView!
    @IBOutlet weak var ring2: UICircularProgressRingView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    @IBAction func deleteButtonTapped(_ sender: Any) {
        
        self.ring1.setProgress(value: 0, animationDuration: 0, completion: nil)
        self.ring2.setProgress(value: 0, animationDuration: 0, completion: nil)
        self.label1.text = ""
        self.label2.text = ""
        
        removeDownloadedItem()
    }
    
    @IBAction func downloadButtonTapped(_ sender: Any) {
        
        let file = "Archive.zip"
        guard let remotePath = URL(string: "http://server.chitas.mobi/\(file)") else { return }

        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let localTextPath = documentsURL.appendingPathComponent("Texts")
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            let fileURL = documentsURL.appendingPathComponent(file)
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
        Alamofire.download(remotePath, to: destination)
                 .downloadProgress { progress in
                    
                    DispatchQueue.main.async { self.updateRingView(progress: progress) }
                  }
                 .response { response in

                    if response.error == nil {
                        
                            guard let zipPath = response.destinationURL?.path else { return }
                            guard let localPath = URL(string: zipPath) else { return }
                        
                            do {
                                try Zip.unzipFile(localPath, destination: localTextPath, overwrite: true, password: "") { (progress) -> () in
                                    
                                    DispatchQueue.main.async { self.updateRingView(progress: progress as AnyObject) }
                                }
                            } catch {
                                print("Something went wrong: \(error)")
                            }
                            
                            do {
                                try FileManager.default.removeItem(atPath: zipPath)
                                print("Archive.zip deleted")
                            }
                            catch let error as NSError {
                                print("Ooops! Something went wrong: \(error)")
                            }
                        
                        
                    } else {
                        print(response.error!)
                    }
            }
            
         }
    }
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // Seting the rings delegate
        ring1.delegate = self
        ring2.delegate = self
        
        // Setting the Rings style
        setUpRing1Style()
        setUpRing2Style()
    }
    
    func setUpRing1Style() {

        ring1.font = UIFont.systemFont(ofSize: 30)
        ring1.ringStyle = UICircularProgressRingStyle(rawValue: 5)!
        ring1.fontColor = UIColor.blue
        ring1.innerRingColor = UIColor.blue
        ring1.innerRingWidth = 10
        ring1.maxValue = 100
    }
    
    func setUpRing2Style() {
        
        ring2.font = UIFont.systemFont(ofSize: 30)
        ring2.ringStyle = UICircularProgressRingStyle(rawValue: 5)!
        ring2.fontColor = UIColor.green
        ring2.innerRingColor = UIColor.green
        ring2.innerRingWidth = 10
        ring2.maxValue = 100
    }
    
    func updateRingView(progress: AnyObject) {
        
        switch progress {
            
        case let progress where ((progress as? Progress) != nil):
            let progress = progress as! Progress
            let valueL = CGFloat(Int((progress.fractionCompleted) * 100))
            let duration = CGFloat(TimeInterval((Int((progress.fractionCompleted) * 100))))
            
            self.label1.text = "Downloading texts"
            self.ring1.setProgress(value: valueL , animationDuration: TimeInterval(duration), completion: nil)
            
        case let progress where ((progress as? Double) != nil):
            let progress = progress as! Double
            let valueL2 = CGFloat(Int((progress) * 100))
            let duration2 = CGFloat(TimeInterval((Int((progress) * 100))))
            
            self.label2.text = "Updating text archieve"
            self.ring2.setProgress(value: CGFloat(valueL2) , animationDuration: TimeInterval(duration2))
            
        default:
            return
        }
        
    }
    
    func removeDownloadedItem() {
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            try FileManager.default.removeItem(at: documentsURL)
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
 
    func finishedUpdatingProgress(forRing ring: UICircularProgressRingView) {

    }
    
}

